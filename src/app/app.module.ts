import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RegionComponent } from './region/region.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { CreateUserProfileComponent } from './userProfile/create-user-profile/create-user-profile.component';
import { EditUserProfileComponent } from './userProfile/edit-user-profile/edit-user-profile.component';
import { ViewUserProfileComponent } from './userProfile/view-user-profile/view-user-profile.component';

import { httpInterceptorProviders } from './authentication/auth-interceptor';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    NavigationComponent,
    RegionComponent,
    RegisterComponent,
    UserComponent,
    CreateUserProfileComponent,
    EditUserProfileComponent,
    ViewUserProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    TableModule,
    ReactiveFormsModule,
    DropdownModule,
    InputTextModule,
    BrowserAnimationsModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
