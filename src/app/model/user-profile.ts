

export class AuthUserProfile {
  id: string;
  userRegion: string;
  userDepartment: string;
  userVile: string;
  userSport: string;
  userPet: string;
  userFood: string;

  constructor(
    userRegion: string, userDepartment: string,
    userVile: string, userSport: string,
    userPet: string, userFood: string,
  ) {
    this.userRegion = userRegion;
    this.userDepartment = userDepartment;
    this.userVile = userVile;
    this.userSport = userSport;
    this.userPet = userPet;
    this.userFood = userFood;
  }

}

